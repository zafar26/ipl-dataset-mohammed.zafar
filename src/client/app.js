function chart1() {
    fetch('./src/output/matchesPerSeason.json')
      .then(response => response.json())
      .then(matches => {
        const year = Object.keys(matches);
        const noOfMatch = Object.values(matches);
        Highcharts.chart('container1', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'Matches Played Per Year'
          },
          subtitle: {
            text: 'Source: Ipl.com'
          },
          xAxis: {
            categories: year,
            crosshair: true,
            title: {
              text: 'Years'
            }
          },
          yAxis: {
            min: 0,
            title: {
                text: 'No Of  Matches'
            }
          },
          series: [
            {
              name: 'Matches Played',
              colorByPoint: true,
              data: noOfMatch,
            }
          ]
        });
      });
  }
  
  

  function chart2() {
    fetch('./src/output/extraRunsConceededPerTeamIn2016.json')
      .then(response => response.json())
      .then(matches => {
        const teams = Object.keys(matches);
        const extraRuns = Object.values(matches);
        Highcharts.chart('container2', {
          chart: {
            type: 'bar'
          },
          title: {
            text: 'Extra Runs Conceeded Per Team In 2016 '
          },
          subtitle: {
            text: 'Source: Ipl.com'
          },
          xAxis: {
            categories: teams,
            crosshair: true,
            title: {
              text: 'Teams'
            }
          },
          yAxis: {
            min: 0,
            title: {
                text: 'Extra Runs Conceeded'
            }
          },
          series: [
            {
              name: 'Extra Runs',
              colorByPoint: true,
              data: extraRuns,
            }
          ]
        });
      });
  }
  
  
  function chart3() {
    fetch('./src/output/tenEconomicBowlersIn2015.json')
      .then(response => response.json())
      .then(data => {
        var bowlers = []
        var bowlerEconomy =[]
        data.forEach(element =>bowlers.push(element[0]))
        data.forEach(element => bowlerEconomy.push(element[1]))
        Highcharts.chart('container3', {
          chart: {
            type: 'line'
          },
          title: {
            text: 'Top 10 Economic Bowlers In 2015 '
          },
          subtitle: {
            text: 'Source: Ipl.com'
          },
          xAxis: {
            categories: bowlers,
            crosshair: true,
            title: {
              text: 'Bowler'
            }
          },
          series: [
            {
              name: 'Economy Rate',
              colorByPoint: true,
              data: bowlerEconomy,
            }
          ]
        });
      });
  }
  

  function chart4() {
    fetch('./src/output/matchesPerSeasonPerTeam.json')
      .then(response => response.json())
      .then(match=> {
        let data=Object.entries(match);

          
        const matchesWonPerYear = data.map(match => ({
        name: match[0],
        data: Object.values(match[1])
      }));
      const years=data.map(match=>Object.keys(match[1]))
      console.log(matchesWonPerYear)

    
      //  console.log(teams)
        Highcharts.chart('container4', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Matches Won By Team Per Year'
          },
          xAxis: {
              categories: years[0]
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'No Of Matches'
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      fontWeight: 'bold',
                      color: ( // theme
                          Highcharts.defaultOptions.title.style &&
                          Highcharts.defaultOptions.title.style.color
                      ) || 'gray'
                  }
              }
          },
          legend: {
              align: 'right',
              x: -30,
              verticalAlign: 'top',
              y: 25,
              floating: true,
              backgroundColor:
                  Highcharts.defaultOptions.legend.backgroundColor || 'white',
              borderColor: '#CCC',
              borderWidth: 1,
              shadow: false
          },
          tooltip: {
              headerFormat: '<b>{point.x}</b><br/>',
              pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
          },
          plotOptions: {
              column: {
                  stacking: 'normal',
                  dataLabels: {
                      enabled: true
                  }
              }
          },
          series:matchesWonPerYear
  
      });
  })
}