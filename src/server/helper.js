const matches = require('../data/matches.json');
const deliveries = require('../data/deliveries.json');


//1st refactor
function matchesPerSeason(matches){
  let matchesSeasonArray = matches.reduce((accumulator, currentValue)=> {
    if(accumulator.hasOwnProperty(currentValue.season) === true){
      accumulator[currentValue.season] += 1
    }else{
      accumulator[currentValue.season] = 1
      }
    return accumulator
    },{})
  return matchesSeasonArray
}
// console.log(matchesPerSeason(matches))







// 2nd Refactor
function matchesPerSeasonPerTeam(matches){
  const years=matches.map(match=>match.season)
  let yearset=new Set(years)
  let yearArray=Array.from(yearset)
  var matchesWon = matches.reduce((accumulator,currentValue)=>{
    if(accumulator[currentValue.winner]){
      if(accumulator[currentValue.winner][currentValue.season]){
        accumulator[currentValue.winner][currentValue.season] += 1
      }else{
        accumulator[currentValue.winner][currentValue.season] = 1
      }
    }
    else{
      accumulator[currentValue.winner] = {[currentValue.season] : 1}
    }
    return accumulator
  },{})
  let emptyyears=yearArray.reduce((a,b)=>{
    a[b]=0;
    return a
  },{})
  matchesWon=Object.entries(matchesWon).reduce((result,winner)=>
  {
    result[winner[0]]=Object.assign({},emptyyears,winner[1])
    return result
  },{})
  return matchesWon;
}


console.log(matchesPerSeasonPerTeam(matches))







// 3rd Refactor
function extraRunsConceededPerTeamIn2016(deliveries){
    var extraRunsConceeded = deliveries.reduce((a,b)=>{
      if(b.match_id >= 577 && b.match_id <= 636){
        if(a[b.bowling_team]){
          a[b.bowling_team] += Number(b.extra_runs)
        }else{
          a[b.bowling_team] = Number(b.extra_runs)
        }
      }
      return a
    },{})
    return extraRunsConceeded
}
// console.log(extraRunsConceededPerTeamIn2016(deliveries))








// 4th Refactor
function tenEconomicBowlersIn2015(deliveries){
    var totalRuns = deliveries.reduce((accumulator,currentValue)=>{
      if(currentValue.match_id >= 518 && currentValue.match_id <= 576){
        if(accumulator[currentValue.bowler]){
          accumulator[currentValue.bowler] += Number(currentValue.total_runs)
        }else{
          accumulator[currentValue.bowler] = Number(currentValue.total_runs)        }
      }
      return accumulator
    },{})
    var balls = deliveries.reduce((accumulator,currentValue)=>{
      if(currentValue.match_id >= 518 && currentValue.match_id <= 576){
        if(accumulator[currentValue.bowler]){
          accumulator[currentValue.bowler] += 1
        }else{
          accumulator[currentValue.bowler] = 1
        }
      }
      return accumulator
    },{})
    var totalRunsPerBowler = Object.entries(totalRuns)
    var ballsPerBowler = Object.entries(balls)
    var economicBowlers = {}
    for(let i=0; i<totalRunsPerBowler.length;i++){
      for(let j=0; j<ballsPerBowler.length;j++){
        if(totalRunsPerBowler[i][0] === ballsPerBowler[j][0]){
          economicBowlers[totalRunsPerBowler[i][0]] = parseInt(totalRunsPerBowler[i][1] / (ballsPerBowler[j][1] / 6))
        }
      }
    }
  return Object.entries(economicBowlers).sort((a,b)=>a[1] -b[1]).slice(0,10)
}
// console.log(tenEconomicBowlersIn2015(deliveries))



module.exports = {matchesPerSeason, matchesPerSeasonPerTeam, extraRunsConceededPerTeamIn2016,
  tenEconomicBowlersIn2015}
