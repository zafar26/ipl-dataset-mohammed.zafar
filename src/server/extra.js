const matches = require('../data/matches.json');
const deliveries = require('../data/deliveries.json');

// 1st
function teamWonTossAndWonTheMatch (matches){
    var teamWon = matches.reduce((accumulator,current_value)=>{
        if(current_value.toss_winner === current_value.winner){
            if(accumulator[current_value.winner]){
                accumulator[current_value.winner] += 1
            }
            else{
                accumulator[current_value.winner] = 1
            }
        }
        return accumulator
    },{})
    return teamWon    
}
// console.log(teamWonTossAndWonTheMatch(matches))




// 2nd
function playersWhoHasWonManOfTheMatchPerSeason(matches){
    var playersOfTheMatch = matches.reduce((accumulator,currentValue)=>{
        if(accumulator[currentValue.season]){
          if(accumulator[currentValue.season][currentValue.player_of_match]){
            accumulator[currentValue.season][currentValue.player_of_match] += 1
          }else{
            accumulator[currentValue.season][currentValue.player_of_match] = 1
          }
        }
        else{
          accumulator[currentValue.season] = {[currentValue.player_of_match] : 1}
        }
        return accumulator
      },{})
      var team = {}
      var number = 0;
      console.log(playersOfTheMatch)
      Object.entries(playersOfTheMatch).forEach(([key, value]) => {
        Object.entries(value).forEach(([a,b]) => {
            if(!(team[key])){
                team[key] = {}
                number = b
            }
            if(!(team[key][b])){
                team[key] = a
                 number = b
            }
                if(team[key][b] > number){
                    team[key] = a
                        number = b
                }
                else{
                    team[key] = a
                    number = b
                }
        });
      });
      return team     
}
console.log(playersWhoHasWonManOfTheMatchPerSeason(matches))








// 3rd
function viratKholiStrikeRate(matches,deliveries){
    var season = matches.reduce((accumulator,currentValue)=>{
        if(accumulator.indexOf(currentValue.season) === -1 || accumulator.indexOf(currentValue.season) === undefined){
            accumulator.push(currentValue.season)
        }
        return accumulator
        },[])
    var data = {}
        for(let i=0;i<season.length;i++){
            var ids = matches.reduce((accumulator,currentValue)=>{
                if (currentValue.season === season[i] ){
                    accumulator.push(currentValue.id)
                }
                return accumulator
                },[])
            var runs =0;
            var balls =0;
            var total =0;
            for(let h =0; h< deliveries.length;h++){
                for(let j=0; j< ids.length;j++){
                    if(deliveries[h].match_id === ids[j] && deliveries[h].batsman === "V Kohli"){
                        runs += Number(deliveries[h].batsman_runs)
                        balls += 1 
                    }
                }                
            }
            total += (runs / balls) * 100
            data[season[i]] = total        
        }
        return data
}
// console.log(viratKholiStrikeRate(matches,deliveries))





// 4th
function playerDismissed(deliveries){
    var a = {}
    for(let i=0; i<deliveries.length;i++){
        if(deliveries[i].player_dismissed !== ""){
            if(a[deliveries[i].player_dismissed]){
                if(a[deliveries[i].player_dismissed][deliveries[i].bowler]){
                    a[deliveries[i].player_dismissed][deliveries[i].bowler] +=1
                }else{
                    a[deliveries[i].player_dismissed][deliveries[i].bowler] = 1
                }
            }else{
                a[deliveries[i].player_dismissed] = {[deliveries[i].bowler] : 1}
            }
        }
    }
    const dismissAsObject = Object.entries(a).reduce(
        (dismiss, players) => {
          dismiss[players[0]] = Object.entries(players[1]).sort(
            (a, b) => b[1] - a[1]
          );
          return dismiss;
        },
        {}
      );
      const mostDismissed = Object.entries(dismissAsObject).reduce(
        (accumulator, currentValue) => {
            accumulator[currentValue[0]] = currentValue[1][0];
          return accumulator;
        },{});
      return mostDismissed;
}
// console.log(playerDismissed(deliveries))




// 5th
function bowlerEconomy(deliveries){
   
    var economicBowlersArray = [];
    var bowlersInDeliveries = deliveries.filter(b=>b.is_super_over ==='1')
    var balls = 0
    for(let i=0; i<bowlersInDeliveries.length;i++){
        if(!economicBowlersArray[bowlersInDeliveries[i].bowler]){
            economicBowlersArray[bowlersInDeliveries[i].bowler] = {};
        }
            if(economicBowlersArray[bowlersInDeliveries[i].bowler]['balls']){
                economicBowlersArray[bowlersInDeliveries[i].bowler]['balls'] += 1
            }
            else{
                economicBowlersArray[bowlersInDeliveries[i].bowler]['balls'] = 1
            }
            if(economicBowlersArray[bowlersInDeliveries[i].bowler]['runs']){
                economicBowlersArray[bowlersInDeliveries[i].bowler]['runs'] += Number(bowlersInDeliveries[i].total_runs)
            }
            else{
                economicBowlersArray[bowlersInDeliveries[i].bowler]['runs'] = Number(bowlersInDeliveries[i].total_runs)
            }             
    }
    var economic = Object.entries(economicBowlersArray).reduce((accumulator,currentValue)=>{
        var total = (currentValue[1]['runs'] / currentValue[1]['balls'])*6
        accumulator[currentValue[0]] = total  
        return accumulator
    },{})
    return Object.entries(economic).sort((accumulator,currentValue)=> accumulator[1] -currentValue[1])[0]
}
console.log(bowlerEconomy(deliveries))

