const call = require('./helper.js')
const fs = require('fs')
const matches = require('../data/matches.json');
const deliveries = require('../data/deliveries.json');

var matches1 = call.matchesPerSeason(matches)
var matches2 = call.matchesPerSeasonPerTeam(matches)
var matches3 = call.extraRunsConceededPerTeamIn2016(deliveries)
var matches4 = call.tenEconomicBowlersIn2015(deliveries)

// // // stringify JSON Object
var jsonContent = JSON.stringify(matches2);

fs.writeFile("../output/matchesPerSeasonPerTeam.json", jsonContent, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
    }
    console.log("JSON file has been saved.");
});